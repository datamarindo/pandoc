---
title: "Intercambio de experiencias en restauración de manglar: Guatemala"
output:
  tufte::tufte_handout:
    citation_package: natbib
    latex_engine: xelatex
  tufte::tufte_html: 
    self_contained: yes
  tufte::tufte_book:
    citation_package: natbib
    latex_engine: xelatex
author: "Pronatura Veracruz"
date: "`r Sys.Date()`"
link-citations: yes
bibliography: biblio.bib
lang: es
urlcolor: blue
linkcolor: blue
header-includes:
  - \usepackage{titling}
  - \pretitle{\begin{center}
    \includegraphics[width=2in,height=2in]{lancha.jpg}\LARGE\\}
  - \posttitle{\end{center}}
  - \usepackage{tocloft}
---



\renewcommand{\figurename}{Fig.}

\newpage

```{r setup, include=FALSE}
library(tufte)
library(dplyr)
library(ggplot2)
library(forcats)

# invalidate cache when the tufte version changes
knitr::opts_chunk$set(tidy = FALSE, cache.extra = packageVersion('tufte'))
options(htmltools.dir.version = FALSE)
```


<!--
\begin{titlepage}
\end{titlepage}
\begin{fullwidth} \end{fullwidth} -->


# Introducción

```{r socios, fig.margin = TRUE, fig.cap = "Instituciones locales participantes", cache=TRUE, message=FALSE, echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/socios.png")
```
Durante el periodo comprendido del 30 de Agosto  al 4 de Septiembre de 2021, personal de Pronatura Veracruz, AC (PVER), realizó una visita a sitios de manglar donde se hayan llevado a cabo acciones de restauración y reforestación de ecosistemas de manglar en la costa pacífica de Guatemala. El viaje surgió a partir de una relación que PVER había entablado con el Instituto Privado de Investigaciones sobre Cambio Climático (ICC). La agenda fue planeada durante los meses de Julio y Agosto 2021 y se determinó incluir cinco sitios a lo largo de la Costa del Pacífico en Guatemala.

La visita fue guiada por  el Ing. Luis Jacob López López, Técnico del Programa Manejo Integrado de Cuencas del ICC; César Zacarías Coxic,  Responsable de Mangle de la Dirección Regional IX, Costa Sur del Instituto Nacional de Bosques (INAB); Érik Ortiz, de Rainforest Alliance y personal de la Comisión Nacional de Áreas Protegidas (Conap) correspondiente a cada sitio visitado  (figs. \ref{fig:socios} y    \ref{fig:mapa_general}, cuadro \ref{cuadro}). El personal de PVER que asistió fue integrado por Charlie García Sosa, técnico de restauración en campo; Ing. Fernando Mota, coordinador en campo y el Dr. Elio Lagunes Díaz, coordinador de ciencia de datos. 


```{r mapa_general, fig.fullwidth = TRUE, fig.margin = FALSE, fig.cap = "Mapa general de sitios visitados", cache=TRUE, message=FALSE, echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/mapa.png")
```

En el recorrido se buscó recoger la información más relevante relacionada al manglar en el sitio: las amenazas, las perturbaciones, cómo llevan a cabo la reforestación en cuanto a financiamiento y diseño metodológico, qué actores participan, cómo vive la comunidad en torno al manglar, entre otras preguntas; para esto se entrevistaba al personal de Conap correspondiente a cada sitio, como autoridades locales, así como al personal del INAB, ICC y Rainforest Alliance que acompañó el recorrido. Las entrevistas partían de un listado de datos cuantitativos relevantes y comunes a las restauraciones: la superficie perturbada/afectada, la composición de especies, la edad de la perturbación/restauración, fuentes financiadoras y actores, para pasar a una conversación horizontal no-estructurada, en la cual encontrar información única y característica de cada sitio/país. Las entrevistas fueron grabadas usando cámaras y teléfonos celulares; paralelamente se documentó con fotografías de cámara y de dron la visita a cada sitio, buscando reflejar el paisaje, la restauración y el entorno en general.

: \label{cuadro} Sitios visitados durante el recorrido.  

| **Fecha de visita**  | **Sitio**                  | **Municipio**         |
| ---------- | -------------------------- | --------------------- |
| 2021-08-30 | Blanca Cecilia             | Iztapa                |
| 2021-08-31 | El Paredón                 | Sipacate              |
| 2021-09-01 | La Blanca                  | San Marcos            |
| 2021-09-02 | Manchón Guamuchal          | Retalhuleu/Champerico |
| 2021-09-03 | Manchón Guamuchal Incendio | Retalhuleu            |

## Instituciones receptoras

El [Instituto Privado de Investigaciones sobre Cambio Climático](https://icc.org.gt/) es una organización dedicada a la investigación, con líneas de trabajo en hidrometeorología, investigación y manejo de inundaciones, gases de efecto invernadero y gestión ambiental; su labor en el país es de tal relevancia que contribuye a los informes nacionales de evaluación ante el Panel Intergubernamental de Cambio Climático. Es financiada por grupos de productores agrícolas e industriales, se ubica Escuintla, en un terreno propiedad del Centro Guatemalteco de Investigación y Capacitación de la Caña (CENGICAÑA); el personal que asistió al recorrido forma parte del programa Manejo Integrado de Cuencas.

El [Instituto Nacional de Bosques es un organismo gubernamental](https://inab.gob.gt), autónomo y con independencia administrativa que funge como la autoridad competente del Sector Público Agrícola en materia Forestal. Su objetivo es promover el desarrollo forestal del país, a través del manejo sostenible y restauración de los bosques, es el equivalente guatemalteco de la Conafor mexicana. Promueve el vínculo entre los bienes y servicioes del bosque con el desarrollo social. El personal que acompañó la visita forma parte de la dirección regional IX de la institución.

[Rainforest Alliance Guatemala]() es una organización internacional que busca proteger los bosques, mejorar los medios de vida de los agricultores y productores forestales, promover derechos humanos y ayudarlos a mitigar la crisis climática. La organización es ampliamente identificada por un sello con el cual certifica productos obtenidos son sostenibles forestal, social, climática y económicamente.

El [Consejo de Áreas Protegidas](https://conap.gob.gt/) es el organismo gubernamental guatemalteco encargada de las 349 zonas de relevancia para la conservación y la diversidad biológica decretadas como áreas protegidas (privadas y públicas), similar en su operación a la CONANP mexicana. En la visita participaron guardaparques de cada área visitada.




\newpage


```{r pano, fig.fullwidth = TRUE, fig.margin = FALSE, fig.cap = "Manglar en Sipacate-Naranjo", cache=TRUE, message=FALSE, echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/panorama.jpg")
```

# El mangle en Guatemala

El Atlas Mundial de los Manglares reporta para Guatemala, cuatro especies de mangle que conforman el ecosistema manglar de los litorales Pacífico y Atlántico, siendo estas: _Avicennia germinans_ , _Conocarpus erectus_, _Laguncularia racemosa_ y en su mayoría _Rhizophora mangle_ (mangle rojo), sin embargo en la _Flora Mesoamericana_ reporta la especie de _Rhizophora racemosa_ para Guatemala. El estudio desarrollado por el Ministerio de Medio Ambiente y Recursos Naturales (MARN) y el Centro del Agua del Trópico Húmedo para América Latina y el Caribe (CATHALAC), en el
2013, concluye que el país cuenta con una extensión de bosque manglar que cubre 17,670.56 hectáreas en el litoral Pacífico, mientras que en el litoral Atlántico se cuenta con una superficie de 1,169.52 hectáreas. El total de esta extensión equivale al 0.17% del territorio nacional y aunque se considera un área relativamente pequeña, desempeña funciones vitales ^[A pesar de ocupar apenas el 0.63% de los bosques de Guatemala, almacena dos veces más carbono que los bosques de latifoliadas y coníferas `r tufte::quote_footer('--- Rodriguez, 2018')`] y de importancia para pobladores cercanos pero también interviene en procesos naturales de otros ecosistemas como el marino-costero que está estrechamente ligado a éste y con repercusiones de mayor impacto para el país (@parcelas).

Guatemala cuenta con la Estrategia Nacional de Restauración del Paisaje Forestal de Guatemala, que estima que existe un área de 10,000 ha que se consideran potenciales para la restauración del bosque manglar. De acuerdo con el análisis de oportunidades de restauración realizado en mangle existen cuatro escenarios donde se plantea realizar restauración o donde debería haber cobertura de mangle: 1) bosque de mangle
ralo y plantaciones; 2) pasto natural y vegetación arbustiva baja; 3) pasto cultivado y vegetación arbustiva baja con flujo hídrico alterado; y 4) cultivos permanentes (@boletin).

* Ley Forestal Decreto Legislativo 101-97
* Ley de Áreas protegidas Decreto Legislativo 4-89
* Ley Reguladora De Las Áreas De Reservas Territoriales Del Estado De Guatemala, Decreto Legislativo 126-97
* Organización y Designación de Funciones de la División de Protección a la Naturaleza de la Subdirección General de Operaciones de la Policía Nacional Civil, Orden General 43-2012
* Ley de Fomento al Establecimiento, Recuperación, Restauración, Manejo, Producción y Protección de Bosques en Guatemala – PROBOSQUE, Decreto No. 2-2015
* Reglamento Para el Manejo Sostenible del Recurso Forestal del Ecosistema Manglar, Acuerdo Gubernativo No. 8-2019



\pagebreak

# 1er visita: Blanca Cecilia 

Se visitó la localidad de Blanca Cecilia, Municipio de Iztapa, al sur de Guatemala. La visita fue guiada por los ing. César Zacarías Coxic y Luis López López. La perturbación de la zona, como la identifica el personal del INAB, se debe al cambio de uso de suelo, principalmente para establecer casas y para la producción de camarones (Fig. \ref{fig:camaroneras}). Otro tema que ejerce presión es la tala ilegal, que es otro factor que ocurre de manera constante; en la zona restaurada que se recorrió durante la visita, los taladores no dejaban ningún árbol en pie al rededor de 2010. En la localidad se llevaron a cabo una serie de procesos de restauración de manglar en el año 2014. Cruzando el río, se encuentra la Reserva Natural Privada de Puerto Viejo.

```{r camaroneras, fig.margin = TRUE, fig.fullwidth = F,fig.show = "hold", fig.cap = "Granjas camaroneras en Blanca Cecilia (arriba); Imagen aérea de restauración (abajo)", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/camaroneras_blanca.JPG")
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/aerea_blanca_cecilia.JPG")
```


En Guatemala existe una figura de gestión social del territorio enfocada al manglar, llamadas __Mesas locales de mangle (MLM)__ donde se agrupan varias comunidades, iniciativa privada, entidades de gobierno, ONGs, locales y en algunos casos internacionales; a nivel de país se tienen __10 mesas locales de mangle__ distribuidas 9 en el pacífico y 1 en el atlántico.  Las MLM, Son una herramienta que impulsa y coordina el INAB para propiciar y fortalecer acciones de protección, conservación, restauración y manejo sostenible de este ecosistema marino costero (@MLM). La restauración en Blanca Cecilia corrió a cargo de la MLM de Iztapa. Cada mesa local de mangle funciona de forma diferente, pero con un mismo fin la conservación, la protección y la restauración de los manglares y de ecosistemas; algunas también trabajan tortugas marinas, iguanas, o con recolección y transformación de la basura. 

```{r rest_blanca, fig.margin = FALSE, fig.fullwidth = F, fig.cap = "Sitio de intervención antes y después de la restauración", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/blanca_cecilia.png")
```


El diseño de la restauración se dividió en dos zonas, de aproximadamente 5 ha cada una (Fig. \ref{fig:rest_blanca}); en la primer zona se llevó a cabo un proceso de rehabilitación natural inducida, donde se propició que el área se recuperara dado que había árboles alrededor, lo cual fomentó el reclutamiento, esta zona tiene notablemente un crecimiento más acelerado (Fig. \ref{fig:bc_rest}); en la otra sección se llevó a cabo una reforestación, a través de un proceso de gobernanza donde participaron estudiantes de nivel básico a partir de tercero de primaria, así como de nivel medio; también participaron comunitarios, así como gente de la zona militar. La reforestación se hizo con distintos diseños, de 0.5 x 0.5 m, de 1x1 m, de 1.5x1.5 m, de 2x2 m, para ver cómo funcionaban. El sitio cuenta con parcelas pemanentes de monitoreo de 3x3 m, agrupadas en parcelas compuestas, que suman 27 m². Se ha observado un crecimiento de 0.6 m a 0.7 m anualmente.

```{r bc_rest, fig.margin = TRUE, fig.fullwidth = F,fig.show = "hold", fig.cap = "Zonas intervenidas: regeneración natural (arriba), reforestada (abajo), nótese la altura de los mangles", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/bc_natural.JPG")
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/bc_rest.JPG")
```


Previo a la intervención se hizo un análisis de la salinidad, se identificó la perturabación y se revisó el hidroperiodo; se encontró que los canales que había de manera natural seguían fluyendo, por lo que se determinó que no había modificaciones hidrológicas. Se identificaron también las especies presentes y la densidad inicial. Tres años después de la intervención los mangles comenzaron a florecer y la producción de propágulos aceleró la recuperación del sistema. De acuerdo con el monitoreo de parcelas, los árboles de la zona de regeneración espontánea crecen 0.7 m anualmente, mientras que los de la zona reforestada crecen entre 0.5 m y 0.6 m anuales; la primera comenzó a fructificar en 2.5 años, mientras que la segunda lo hizo en 3.5 años. La sobrevivencia ha sido de un 99%; mientras que actualmente comienza un problema de tala, porque los árboles ya han alcanzado una talla suficiente para el aprovechamiento.


> Inicialmente, fíjense que empezamos a ver cuánto nos costaba, aquí hicimos un proceso de estimación que nos arrojó de __Q 33,000 ha__ [US 4,285/ha]. Ese es el costo que nosotros calculamos poniéndole un valor a __todo__ lo que hicimos; sin embargo, si usted tuviera  ese fondo para invertir acá, no lo tuvimos nunca; aquí fue mano de obra voluntaria de todos los que participaron, los propágulos fueron donados, las camaroneras dieron gasolina, alimentos, el ejército puso mano de obra para la recolecta, las comunidades pusieron tanto, así sumamos todo lo que se necesitaba para poder hacer esto; extrajimos todo el material que había dispuesto que eran todos los restos de los árboles que habían cortado. Eso fue lo más caro, realmente, porque hacer la reforestación no implica mucho esfuerzo, pero entrar con una motosierra encima, 
es complicado.
>
> `r tufte::quote_footer('--- César Zacarías Coxic')`


En Guatemala existe una normativa de restauración del paisaje forestal, que es similar a la restauración ecológica, pero con mayor énfasis en el monitoreo de fauna.

\pagebreak

# 2da. visita: El Paredón

```{r paredon, fig.margin = TRUE, fig.fullwidth = F,fig.show = "hold",fig.cap = "Visita con el personal de CONAP de Sipacate-Naranjo, traslado (arriba) y en la zona restaurada (abajo), nótese la densidad de la siembra", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/IMG-20210831-WA0019.jpeg")
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/IMG_20210831_144305.jpg")
```

El segundo sitio del recorrido fue El Paredón, dentro del Parque Nacional Sipacate-Naranjo, ubicado en la localidad Naranjo, Municipio de Sipacate. En el lugar el recorrido fue guiado por Carlos Velázquez, de la Conap (fig. \ref{fig:paredon}. El sitio cuenta con varias restauraciones menores a una hectárea, realizadas en 2017 (fig. \ref{fig:paredon_rest}). Una particularidad de esta restauración es que la siembra fue realizada por mujeres: madres solteras y viudas, quienes trabajaban a cambio de víveres, mientras que la recolecta de propágulos fue llevada a cabo por la Conap, en coordinación con el ICC. El área de
conservación Sipacate Naranjo, no es un área protegida legalmente, desde el año 2000 el CONAP administra la zona (@carbono_sipacate).

Las principales amenazas al manglar, como las reconoce Carlos Velázquez, son los fenómenos naturales: vientos fuertes en invierno, rayos que llegan a quemar el manglar. No se perciben los incendios provocados como una acción que ocurra en el sitio. En la zona corre el canal artificial de Chiquimulilla, creado en 1870 con el propósito de mover mercancias entre la frontera con El Salvador y el Puerto San José (@chiquimulilla); este canal suele verse afectado por mangles que el viento tira, cuando esto sucede, Conap se encarga de la limpieza. Como estrategia de reforestación, la Conap permite a las comunidades aprovechar la madera de mangle de las zonas siniestradas, así llevan a cabo las acciones de limpieza de sitio donde se harán las siembras; durante la conversación apuntaron que no creen que la comunidad provoque siniestros para poder hacerse de leña.

```{r paredon_rest, fig.margin = FALSE, fig.fullwidth = T, fig.cap = "Parcelas restauradas", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/paredon.png")
```


Una estrategia interesante de diseño de restauración que emplean en el sitio es la de sembrar a densidades muy altas, __15 cm, o 444,448 plantas por hectárea__; el propósito de este diseño es que la misma densidad impida el paso a los taladores (fig. \ref{fig:densidad}). Sin embargo, las comunidades han manifestado su inconformidad ante este diseño. En contraste, los diseños de las restauraciones se hacen exclusivamente con mangle rojo, que es la especie más aprovechada.

De acuerdo con las autoridades, no se tiene el registro del año de las perturbaciones, estas fueron detectadas como parte de un programa de identificación de sitios para limpieza.

> Hemos tenido problemas con los COCODES (Consejo Comunitario de Desarrollo) quizá es la parte de las comunidades que hay dentro del área, donde lo utilizan no solo para rancho, sino también para leña, que lastimosamente se utiliza mucha leña. Los COCODES dicen que es de ellos el manglar y que hay que aprovechar y es lo que se nos complica.
>
> `r tufte::quote_footer('--- Carlos Velázquez')`

```{r densidad, fig.margin = T, fig.fullwidth = F, fig.cap = "Reforestación densa", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/IMG_20210831_143726.jpg")
```

Se reconoce que en el bosque de manglar se estań abriendo otros campos compatibles con la conservación, tales como el aviturismo; también consideran que el manglar es importante para la pesca. En la zona cuentan con arribos de tortugas, la tortuga parlama (_Lepidochelys olivacea_), que llega a la playa y la tortuga negra (_Chelonia agazzisii_) hacia la Laguna del Nance. 

Según Rodríguez y Ramírez (@carbono_sipacate), en Sipacate, el 66% de la población se dedica a la pesca y agricultura, para quienes el manglar representa un aporte económico por medio del comercio de madera en las áreas costeras, además del cambio de uso del suelo para el establecimiento de potreros o parcelas para agricultura, y el establecimiento de camaroneras y salineras. De acuerdo a estos autores, en la zona el balance de pérdidas y ganancias de la cobertura del mangle ha sido positivo, puesto que en el período 1990-2006 la ganancia neta fue de 163.95 ha (8.38%) y en el período 2006-2016 fue de 46.88 ha (2.31%)




\pagebreak

# 3er. visita: La Blanca


```{r la_blanca, fig.margin = TRUE, fig.show = 'hold', fig.fullwidth = F, fig.cap = "Parcela de restauración (arriba); plantación de botoncillo (abajo)",cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/parcela_la_blanca.png")
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/botoncillo.jpg")
```

El sitio de La Blanca, municipio de San Marcos, se ubica a menos de 5 km de la frontera con México, en la parte norte del Manchón Guamuchal, Área de Protección Especial decretada en 1998, sitio Ramsar desde 1995 y área de importancia para las aves y biodiversidad (IBA) (@wdpa). La restauración de este sitio (fig. \ref{fig:la_blanca}), de 6.28 ha, realizada en diferentes años, fue llevada a cabo por comunitarios, con un importante aporte de estudiantes de la [`Universidad de San Carlos`](https://radiotgw.gob.gt/realizan-reforestacion-de-mangle-rojo-en-san-marcos/), la Universidad Rural y Colegios de educación a nivel medio, coordinados por INAB y CONAP. Estas acciones recibieron apoyo del [IDESAC](https://www.idesac.org/). En la zona el mangle rojo es ampliamente utilizado para construcciones, lo cual, junto con el uso como dendroenergético y el cambio, [ilegal a veces](https://www.prensalibre.com/ciudades/san-marcos/senalan-supuesta-entrega-lotes-0-1083491681/), de uso de suelo. En la zona, nos recibieron Danilo Jiménez y Samuel Estacuy como representantes de la CONAP.

En la restauración se llevó a cabo un programa de podas para propiciar la ganancia de altura en los manglares; en este proceso se observó que las plantas que no tenían tratamiento crecían alrededor de 0.4 m a 0.6 m anualmente, mientras que los sujetos de eliminación de rama tenían incrementos de entre 0.6 m y 0.9 m, por lo que a los dos o tres años ya eran sujetos de aprovechamiento.

Para la construcción de palapas en la región se utilizan tres categorías diamétricas de madera de manglar: la varilla, de una pulgada de diámetro, que sirve para hacer el entrelazado del techo, donde se sujetan las hojas de palma; el calsonte (del nahua _calli_, casa, y _tzontli_, cabello, pelo) [RAE](https://www.rae.es/damer/calsonte), una pieza de cinco pulgadas, que es la tijera o armazón que sostiene la techumbre y, finalmente la viga, de cuatro pulgadas, con la que se construye el marco, que se apoya en los calsontes. La varilla, según los datos dasométricos recolectados en el monitoreo, se obtenía a los dos años en la zona sujeta a podas.

Una experiencia adquirida en este sitió que permeó la conservación de manglar a nivel país, fue la de buscar respetar la composición del bosque, de no hacer una reforestación que propicie cambios de especie, sobre todo por el valor que tienen los mangles rojo, blanco y botoncillo (fig. \ref{fig:la_blanca}) , en comparación con el negro, el cual no es apreciado y tiende a ser reemplazado por rojo. El trabajo con las comunidades y la naturaleza de los apoyos hace que esta premisa necesite ser implementada mediante convencimiento con los actores locales. Otra experiencia que se recoge de aquí, a diferencia de los primeros sitios, es que no se hace limpieza de la zona, es decir, ya no se usa remover los árboles talados, pues según el INAB esto representaba casi el 90% de la inversión total.

Como parte del programa de parcelas de monitoreo permanente de manglares, el cual cuenta con 15 parcelas dentro de la zona, se ha observado que en los primeros años de la regeneración natural se forman conglomerados de plántas, que es el patrón de distribución en función del agua, de los sedimentos y de la especie, que alcanzan densidades de entre 160,000 hasta 190,000 plantas por hectárea, es decir, entre 10, 17 y hasta 19 plantas por metro cuadrado. 

Durante el recorrido se comentó que el INAB también lleva un registro de enfermedades del manglar, entre los cuales se encuentran  insectos xilófagos, de familias tales como _Cerambicidae_ y _Xileborus_, hongos de las familias _Fusarium_, _Sclerotium_ y _Cylindrocarpon_ y bacterias _Agrobacterium_ (fig. \ref{fig:agrobacterium}). También, durante este recorrido, se visitó una zona (fig. \ref{fig:pano}) en la cual se encuentra un puente creado con el propósito de desarrollar actividades ecoturísticas dentro de los manglares de la zona.



```{r agrobacterium, fig.margin = FALSE, fig.fullwidth = F, fig.cap = "Mangle con infección bacteriana", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/agrobacterium.jpg")
```


\newpage 

# 4ta. visita: Manchón Guamuchal

```{r manchon, fig.margin = TRUE, fig.fullwidth = F, fig.cap = "Imagen del recorrido ", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/IMG-20210902-WA0028.jpg")
```

Se dice que los pobladores iban a ese lugar y observaron que ahí crecían árboles de guamuch (_Pithecellobium dulce_). Así mismo, era común ver a las aves migratorias pasar y formar un manchón o mancha; por ese motivo decidieron nombrarlo como el Manchón Guamuchal. _Esto pone de manifiesto su valor para las aves migratorias_.


[Aprende Guatemala](https://aprende.guatemala.com/historia/geografia/manchon-guamuchal-retalhuleu/)
[Salvemos al Manchón](https://salvemoselmanchon.org/)




El sistema de tenencia de la tierra del Manchón Guamuchal está dominado por la propiedad privada, que representa el 80% del humedal, mientras que 15% es propiedad del estado y solo 5% pertenece a comunidades locales; en esta distribución de la propiedad se refleja el hecho de que Guatemala es uno de los países de América Latina con la distribución más inequitativa de la tierra, `r tufte::margin_note("En Guatemala existe una figura jurídica para propiciar la reforestación: el cumplimiento de resarcimiento de daños, que se enfoca en personas que han cometido algún delito ambiental (tala ilegal), situación en donde la fiscalía de delitos contra el ambiente del Ministerio Público otorga una oportunidad por el tipo de delito cometido; algunas de las reforestaciones dentro del Manchón Guamuchal han ocurrido bajo este esquema --- Hdz. Glz. [2019]") ` donde aproximadamente el 80% de la tierra arable se encuentra en posesión de menos del 5% de la población. Las principales actividades económicas de estas comunidades incluyen pesquerías artesanales y trabajos temporales en las plantaciones de caña, plátano y en las granjas camaroneras; también el sector turístico tiene una participación importante, aunque mucho menor, en la economía de la región.
El involucramiento de las comunidades locales ha sido clave para la conservación, gracias a que ha creado un sentido de propiedad y ha propiciado un impulso para cuidar los recursos de la reserva. (@governance)



```{r manchon_rest, fig.margin = FALSE, fig.fullwidth = F, fig.cap = "Zona restaurada ", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/IMG-20210924-WA0006.jpg")
```

\pagebreak
# 5ta. visita: Manchón Guamuchal, incendio

```{r manchon_incendio, fig.margin = TRUE, fig.fullwidth = F, fig.cap = "Imagen de la zona devastada", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/DJI_0585.JPG")
```

```{r manchon_incendio_mapa, fig.margin = TRUE, fig.fullwidth = F, fig.cap = "Extensión del incendio", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/incendio_manchon.png")
```



\pagebreak
# Las tecnologías de la información en la conservación del manglar

```{r portal, fig.margin = TRUE, fig.fullwidth = F,fig.show = "hold", fig.cap = "Portal de monitoreo de mangle INAB (arriba) y portal de incentivos forestales INAB (abajo)", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/monitoreo_mangle_inab.png")
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/incentivos_forestales.png")
```

Una fortaleza con la que cuenta el monitoreo de manglares en Guatemala es el desarrollo de aplicaciones de recolecta, almacenamiento y presentación de datos; en el INAB cuentan con un geoportal donde se puede consultar información de las Parcelas Permanentes de Monitoreo (PPM) de Manglares, así como de Incentivos Forestales (@geoportal_inab); estos sitios son alimentados con aplicaciones móviles de recolección de datos creadas con la herramienta ArcSurvey (@arc_survey). Estas aplicaciones, cuyas licencias para organizaciones del tamaño del INAB alcanzan preciosd e millones de dólares, son provistas como parte de la cooperación con el World Resources Institute, junto con otras licencias para sistemas de información geográfica y creación de ortomosaicos de imágenes de dron. Este equipamiento en software hace que el INAB se encuentre actualizado con las tendencias de manejo de información y automatización de procesos.


En comparación, Pronatura Veracruz también se apoya en una gran medida en aplicaciones y sistemas de almacenamiento de información, cuya principal diferencia con el caso guatemalteco es el uso de herramientas de software libre: OpenDataKit (@odk) para recolección de datos, GeoServer (@geoserver) para creación de aplicaciones geoespaciales para presentar datos y OpenDroneMap para producción de ortomosaicos de imágenes de dron. Estas herramientas, a diferencia de las utilizadas por el INAB, tienen requerimientos, aunque básicos, de personal de informática para la configuración, pero tienen la ventaja de un control total de la información por parte de los usuarios.



```{r apps, fig.margin = FALSE, fig.fullwidth = F, fig.cap = "Impresiones de pantalla de aplicaciones móviles usadas por el INAB", cache=TRUE, message=FALSE,echo = FALSE, out.width='100%'}
knitr::include_graphics("/home/pronatura/2022-01-02-0005-backup/guatemala/seleccion/rapps.png")
```

# La industria privada en la conservación
## (hablar de que el ICC lo financian cañeros, de la asociación de aceite de palma que financia a salvemos_el_manchón)


\pagebreak


\tableofcontents
\listoffigures

\newpage