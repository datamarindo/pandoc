---
title: "Informe de actividades realizadas como parte del proceso de actualización de IBAs en México"
author: Elio Lagunes Díaz, Efraín Castillejos Castellanos
geometry: margin=2.3cm
bibliography: informe_bibtex.bib
---

\renewcommand{\figurename}{Fig.}
\renewcommand{\tablename}{Tabla}
\renewcommand{\contentsname}{Contenido}
\renewcommand{\listtablename}{Lista de tablas}
\renewcommand{\listfigurename}{Lista de figuras}

\tableofcontents
\listoftables
\listoffigures

---

## Introducción

     Las Áreas de Importancia para las Aves y la Biodiversidad son zonas delimitadas geográficamente, de alto valor para la conservación de especies de aves, ya sea porque albergan especies amenazadas o endémicas o porcentajes altos de la población total de una especie. La autoridad encargada de decretar las IBAs es BirdLife, en conjunto con socios nacionales en los distintos países, tal es el caso de Pronatura en México. Las IBAs llevan un proceso periódico de actualización, a lo que obedece el presente trabajo, llevado a cabo durante los meses de Diciembre 2020 a Junio 2021. El proyecto fue realizado por un equipo conformado por personal de Pronatura Sur y Pronatura Veracruz, asistido por personal de Birdlife [@birdlife]. 

     El proceso incluyó una comparación-revisión de los límites entre los polígonos de las IBAs y las AICAs, seguido por una homologación de los nombres científicos usados en los registros biológicos y en los listados de los criterios de BirdLife. El cumplimiento de dichos criterios fue revisado utilizando distintas metodologías cuyos propósitos principales eran **1)** Evitar sobreestimar las poblaciones;  **2)** Utilizar la mayor cantidad de datos disponibles; **3)** Que fuera un proceso replicable, escalable, programático y auditable.

     Una complicación mayor que se encuentra al usar datos de eBird es el tamaño de los archivos, que pasan los millones de registros y superan los 4 Gigabytes para el caso de México, por lo cual no se pueden procesar en software como excel, por ejemplo, porque tiene un límite de 1 millón de registros por hoja; su volumen hace también difícil procesarlo en `Python` o en `R` en sus módulos básicos, porque acomodan la base de datos en la memoria RAM. Por lo cual el proceso se diseñó usando `csvkit` [@csvkit] para la limpieza de duplicados, `ogr2ogr` [@gdal] para conversión a formato geoespacial y pre-proceso de variables y `QGIS` [@qgis] para filtrar los registros dentro de las IBAs. Después de esto, el proceso fue llevado a cabo en `R` [@rbase].

## Materiales y metodología

     Los polígonos de las Áreas de Importancia para la Conservación de las Aves (AICA) fueron obtenidos del geoportal de Conabio [@aicas], mientras que los polígonos de las IBAs fueron proporcionados por BirdLife [@birdlife], además se agregaron 14 polígonos de áreas de aves acuáticas para la su consideración como IBAs nuevas. Además de los polígonos mencionados, se utilizaron los de AICAs que previamente no habían calificado como IBAs, para revisar si era posible que calificaran en este proceso.

     Los registros biológicos provinieron principalmente de la _eBird Basic Dataset_ [@ebird], así como del monitoreo Western Mexico Waterbird Survey (2011-2020) (proporcionados por Eduardo Palacios de CICESE) [@eduardo], del monitoreo de aves acuáticas de Pronatura Noroeste (2013-2017) [@pronor] y del programa Veracruz Río de Rapaces (2010-2020) de Pronatura Veracruz [@vrr].

     La información de categorías de riesgos, tamaños poblacionales globales y tamaños de rango geográfico por especie fueron provistos por BirdLife [@birdlife], así como los umbrales poblacionales requeridos por los criterios para ser consideradas como IBAs. Antes de la revisión de los criterios se hicieron una serie de preprocesos para hacer los datos compatibles y garantizar que estuvieran correctos.

### Preproceso geoespacial

     Antes de unir geográficamente el polígono de la IBA a los registros geográficos que cayeran dentro del límite de una, se llevó a cabo una revisión de los límites, comparando los de Birdlife con los de AICAs de Conabio, reconociendo a la segunda como la autoridad dentro de México; también se compararon con las zonas geográficas a las que se refieren, por ejemplo, se revisó que la IBA Lago de Texcoco siguiera los límites del cuerpo de agua de ese mismo nombre. Para facilitar y agilizar esta tarea se hizo un mapa interactivo, que permitía buscar por nombre las IBAs y visualizar ambos polígonos, junto con una imagen satelital [@google] para revisar que cubriera el área de interés, el mapa está disponible en: https://pronaturaveracruz.org/recursos/aicas_ibas.html

     Después de este proceso se encontró que muchas IBAs presentaban un desfase ligero, de entre 50 m y 90 m, respecto de la AICA de su mismo nombre, probablemente originados por una transferencia y conversión de datos entre instituciones y personas dentro de las instituciones (fig. \ref{desfase}); otras problemáticas se encontraron en IBAs donde BirdLife realizó una ampliación de límites (fig. \ref{modificado}), que los límites no coincidían con su área de interés o que cruzaban fronteras internacionales (tabla 1).

![Desfase ligero entre polígonos: AICA (rojo) e IBA (azul), Isla Guadalupe\label{desfase}](figs/141a.png "li"){ width=15% }

![Modificación de polígono por BirdLife: AICA (rojo) e IBA (azul), Ría Lagartos\label{modificado}](figs/186a.png "lix"){ width=25% }

\pagebreak
:  Resumen de problemáticas detectadas como parte del proceso de revisión de polígonos

Problemática | cantidad
-------------|---------:
Desfase ligero | 105
Ampliación Birdlife | 10
No coincide con cuerpo de agua | 4
Cruza fronteras | 4


### Preproceso base de datos eBird

     La base utilizada fue al corte de Agosto 2020 (ebd_MX_relAug_2020), la cual es un archivo separado por tabuladores con 10,151,291 registros. De esta base solo se usaron aquellos registros a partir de 2010, cuya categoría fuera "species" para no usar subespecies o aves solo identificadas a nivel de género, este primer filtro, junto con la transformación a formato separado por comas, fue realizada utilizando el paquete `csvkit` [@csvkit]; posteriormente se eliminaron los registros duplicados, conservando un solo registro por cada conjunto **nombre cientifico/fecha/conteo/longitud/latitud**[^1], incluso de registros con distinta hora de observación se retendría solo un valor; esta fase, junto con la conversión a formato geoespacial fue realizada con el paquete `ogr2ogr` [@gdal].  Después de aplicar los filtros y eliminar duplicidades se retuvieron 5,731,192 registros. A los registros filtrados que cayeran dentro de un polígono de una IBA se les unió el nombre y la ID de IBA, utilizando el software `QGIS` [@qgis] mediante la función `unir atributos por ubicación` . Todos los programas utilizados en este paso no requieren cargar los archivos al RAM (el archivo __ebd__ pesa 3.6 GB y puede duplicar este volumen una vez leído en `R` o en `Python` usando las librerías estándares), lo cual hace replicable esta fase en computadoras de pocos recursos; la ejecución de todo este paso toma entre 20 y 30 minutos en una computadora de recursos medios.

[^1]: Para este proceso no se usaron las columnas "GROUP IDENTIFIER" ni "IBA CODE", puesto que la primera podía eliminar registros en distintas fechas y lugares, además de estar presente en solo 2,350,595 (23%) de los registros totales, mientras que no en todos los registros dentro de una IBA se incluía el código de IBA en la que ocurrían.


### Preproceso taxonómico

     Para poder revisar los criterios de BirdLife se requería de una homologación de los nombres taxonómicos, puesto que existían numerosas diferencias, con especies no enlistadas o no actualizadas en cada una de las bases, tales como _Tangara cabanisi_, de la lista de BirdLife que en eBird, la checklist de la American Ornithologist Union y el listado Aves de México de Conabio aparece como _Poecilostreptus cabanisi_, u _Oceanodroma castro_, que aparece como tal en eBird, mientras que en las demás autoridades taxonómicas se reconoce como _Hydrobates castro_. Para hacer esto se programó una consulta iterada en Avibase sobre los nombres científicos de eBird, utilizando el lenguaje de programación _R_ y se generó una tabla con los nombres usados por cada autoridad, a la cual además después se le añadieron los nombres taxonómicos utilizados por Pronatura Noroeste y el Western Mexico Waterbird Survey, ya que estaban desactualizados.

\pagebreak

### Criterio A1: Especies globalmente amenazadas

     Este criterio aplica a especies que se encuentren dentro de las categorías "críticamente amenazada" (CR, critically endangered), "En peligro" (EN, endangered) y "vulnerable" (VU, vulnerable) de la lista roja de la Unión Internacional para la Conservación de la Naturaleza (IUCN). El criterio define umbrales para pares o individuos, en este caso, como no se recaba información del sexo de los individuos se utilizaron solo los umbrales para individuos, resumidos en la siguiente tabla (2).

: Umbrales requeridos para el criterio A1

Categoría | Individuos requeridos
---|---:
CR|15
EN|15
VU|30
CR, pob. <= 1,500 individuos|1
EN, pob. <= 1,500 individuos|1

### Descripción del análisis geoespacial para evitar posibles duplicidades

     Para evitar la duplicidad en registros de un posible mismo individuo, más allá de la fecha y localización, se desarrolló un método que consistía en tomar para cada año los registros de cada especie en una IBA, generar un buffer de una distancia que dependía del grupo biológico, fusionar estos buffers en un solo polígono y tomar de entre todos estos registros aquel con el valor máximo de la variable OBSERVATION COUNT, y finalmente sumar todos estos máximos de los distintos polígonos; este proceso fue realizado con el paquete `sf` [@sf]. La distancia del buffer fue propuesta por Efraín Castillejos como experto ornitológico para cada ave en las categorías de riesgo referidas anteriormente. El proceso se ilustra en la siguiente secuencia de figuras para un mismo año; en este caso el valor reportado sería 18 (2 + 1 + 2 + 6 + 7) para _O. derbianus_ en la IBA El Triunfo (figs. \ref{registros} y fig. \ref{maximos}).

![Registros, 2013\label{registros}](figs/ebird1_a.png "lin"){ width=50% }

![Polígonos disueltos con máximos, 2013\label{maximos}](figs/ebird3_a.png "lts"){ width=50% }

\pagebreak

### Criterio A2: Especies de rango restringido

     Este criterio, a diferencia del A1, es aplicable a especies de cualquier categoría de riesgo, incluidas "casi amenazadas" (NT, near threatened) y "de preocupación menor" (LC, least concern); su enfoque son aves de rangos geográficos menores a 50,000 km^2^. El umbral requerido es igual al 1% de la población global estimada para la especie; requiere la presencia de al menos dos especies cuyas poblaciones superen los umbrales. El proceso para evitar duplicidades fue igual al de la categoría A1. La siguiente tabla (3) es un ejemplo de los rangos poblacionales requeridos por especie para este criterio.

: Ejemplo de umbrales de 1% de la población global

Especie | 1% población global
---|---:
_Aegolius ridgwayi_ |	350
_Campylopterus rufus_	| 350
_Campylorhynchus chiapensis_ | 350
_Cardellina versicolor_ |	350
_Oreophasis derbianus_ | 17
_Poecilostreptus cabanisi_ | 175


### Criterio A4: Congregaciones

     Se toman como congregaciones, al igual que el criterio A2, los sitios que concentren regularmente más del 1% de la población global de una especie; en el criterio A4 no se toma en cuenta el rango de las especies ni la categoría de IUCN de las especies. Para revisar este criterio se utilizaron, además de los datos de eBird, registros de Pronatura Noroeste, Pronatura Veracruz y el Western Mexico Bird Survey (WMBS). Los datos de estas fuentes fueron provistos además junto con información geoespacial de los sitios de estudio.

     En el caso de los datos de Pronatura Noroeste se consolidó una base de datos con los máximos por sitio de estudio, posteriormente se filtraron geoespacialmente aquellos sitios que caían dentro de IBAs; de los registros del WMBS se utilizaron aquellos de sitios de estudio que tuvieran más del 50% de su superficie dentro de una IBA, para después sumar por cada año en cada sitio los conteos de cada especie (previa consulta con el equipo de CICESE sobre la aplicabilidad); los datos de Pronatura Veracruz corresponden a la IBA Centro de Veracruz, se tomaron los máximos de cada especie monitoreada para su comparación con los umbrales; finalmente, los datos de eBird fueron filtrados a solamente los máximos conteos de cada especie por IBA, es decir, no se hacían sumatorias de ningún periodo ni por regiones dentro de una misma IBA.

\pagebreak

### Criterio B1

     El criterio B1 es semejante al A1, para especies en categoría NT, y sus umbrales son 90 individuos para paserinas y 30 para no paserinas. De igual manera se propusieron distancias para evitar las duplicidades conservando los conteos máximos dentro del rango espacial.

### Criterio B3a

     El criterio B3a se refiere a las poblaciones biogeográficas, es decir, poblaciones de una misma especie que pasan alguna etapa del año en regiones distintas. Los datos de las distribuciones espaciales de las poblaciones biogeográficas fueron provistos por Wetlands International [@wetlands]; la información de estos polígonos fue geoespacialmente unida a los registros de eBird, de acuerdo a la etapa del ciclo reproductivo (breeding y non-breeding) en la que se ubicaban en la zona. En los casos de traslape no se consideron esos registros para el proceso de revisión de IBAs. La figura 5 muestra un ejemplo de poblaciones biogeográficas.

![Ejemplo de poblaciones biogeográficas\label{biogeograficas}](figs/biogeog.png "lin"){ width=50% }

### Criterio B3b

     Este criterio se refiere a congregaciones de aves acuáticas, el umbral para cubrirlo son 20,000 aves en cualquier época del año. Para este efecto se tomó el Observation Count máximo de cada especie de cada año por IBA y se sumaron. Para el reporte se subieron las especies suficientes para cubrir el umbral, es decir, si una sola especie tenía un Observation Count superior al umbral, solo esa se incluía; en cambio, si el umbral se alcanzaba con la suma de 10 especies, se incluían todas.

### Consolidación de información

     Para facilitar la subida a la base World Bird Data Base de BirdLife se consolidaron todas las bases de cada criterio y cada fuente de registros en un solo archivo, además de la cantidad de individuos observadas por especie, BirdLife pide una ponderación cualitativa de abundancia de la especie, para este requisito, para cada combinación IBA-especie se calculó el total de observaciones de la especie y se agruparon en sextiles de acuerdo a su total de observaciones, cada sextil se asignó a su respectiva categoría dentro de las seis posibles: _abundant, Frequent, Common, Present, Rare, Uncommon_ (Abundante, frecuente, común, presente, rara, poco común) además de _Unknown_ (desconocido), la cual se asignaba a IBAs con menos de 10 observaciones en total (tabla 4). 


: Ejemplo de formato para subida a la WBDB

Nombre científico| Máximo|Año|Abundancia|Categoría IUCN|Umbral|Fuente|Criterio
---|---:|---|---|---|---:|---|---:
_Charadrius semipalmatus_| 2,565|2012|Abundant|LC|1,500|WMBS|A4
_Charadrius wilsonia_| 500|2012|Abundant|LC|60|ebird|A4
_Dendrocygna autumnalis_| 25,000|2012|Abundant|LC|15,500|ebird|A4
_Eupsittula canicularis_| 87|2015|Abundant|VU|30|ebird|A1
_Charadrius nivosus_| 473|2017|Abundant|NT|413|WMBS|A4
_Pelecanus erythrorhynchos_| 5,000|2017|Abundant|LC|1,800|ebird|A4
_Recurvirostra americana_| 35,659|2017|Abundant|LC|5,500|ProNor|A4
_Himantopus himantopus_| 12,890|2017|Abundant|NA|6,150|ProNor|A4
_Limosa fedoa_| 4,018|2017|Abundant|LC|1,700|ProNor|A4
_Limnodromus griseus_| 3,050|2018|Abundant|LC|2,500|ebird|A4


### Resultados

     Como parte del proceso de revisión de IBAs se lograron cumplir los criterios de 137 IBAs; 67 cumplieron con el criterio A1, 1 con el A2, 58 con el A4, 89 con el B1, 65 con B3a y 22 con B3b. 47 cumplieron con un solo criterio, mientras que 90 cubrieron con 2 o más. Centro de Veracruz, Estero Santa Cruz, Istmo de Tehuantepec - Mar Muerto, Laguna de Bustillos y Marismas Nacionales son las cinco IBAs que cubrieron cinco criterios; solo Islas Revillagigedo cubrió el criterio A2.

La sig. tabla (3) ennumera la cantidad de IBAs que pasa por cada criterio.

: Combinaciones de criterios y número de IBAs que los cubren

| criterio         | no. de ibas | criterio | no. de ibas |
|------------------|-------------|----------|-------------|
| A1 A4 B1 B3a B3b | 5           | A1 B3a   | 3           |
| A4 B1 B3a B3b    | 8           | A4 B3a   | 3           |
| A1 A4 B1 B3a     | 6           | B1 B3a   | 9           |
| A1 A2 A4 B1      | 1           | A1 A4    | 7           |
| A1 B3a B3b       | 1           | A1 B1    | 15          |
| A4 B3a B3b       | 5           | A4 B1    | 5           |
| B1 B3a B3b       | 2           | B3a      | 5           |
| A1 B1 B3a        | 3           | A1       | 23          |
| A4 B1 B3a        | 14          | A4       | 1           |
| A1 A4 B1         | 3           | B1       | 18          |
| B3a B3b          | 1           |          |             |



## Otras actividades

### Mapa interactivo de IBAs con los polígonos resultantes del proceso de limpieza

     Para mostrar en un mapa los avances al donante se preparó un mapa interactivo (fig. \ref{leaflet}) con los polígonos enviados a BirdLife como parte del proceso de revisión de límites, en el cual se mostraban la máximas categoría de riesgo de las especies de la IBA. El mapa fue subido al servidor, con las imágenes institucionales del Programa Nacional Aves de México de Pronatura y de BirdLife; el mapa se encuentra disponible en la liga: https://pronaturaveracruz.org/recursos/ibas_mex/.

![Mapa interactivo de IBAs\label{leaflet}](figs/ibas_leaflet.png "leaflet"){ width=30% }


### Descarga de directorio Comisión Nacional de Áreas Protegidas

     Como parte de otro producto de la consultoría se descargaron programáticamente todos los datos del Directorio de la Comisión Nacional de Áreas Protegidas (Conanp), para integrarse en un documento de contactos de IBAs, en los casos de que la IBA corresponda con un Área Natural Protegida federal. El directorio se encuentra en la liga: https://directorio.conanp.gob.mx/directoriolist.php.


## Aprendizajes del proceso

     La actualización de los datos de las IBAs mexicanas es el resultado de un proceso colectivo hecho posible principalmente gracias a las herramientas de ciencia ciudadana disponibles a través de eBird, así como a monitoreos establecidos y sistemáticos, tales como los de aves de humedales o Veracruz Río de Rapaces. Entre todos los actores que contribuyen datos a las plataformas o que los compartieron directamente con nosotros, hicieron posible el cumplimiento en 260% de la primer meta de actualizar 50 IBAs, para llegar a 137.

     La traducción de los criterios de BirdLife al lenguaje SQL que entiende la computadora (structured query language) fue un proceso lineal, una vez entendida toda su dimensión por parte del programador; las capacidades necesarias para recrear este proceso (por el lado de tecnologías de información) se encuentran muy disponibles en cada país de latinoamérica, el obstáculo principal es el desconocimiento de este tipo de trabajos por parte de los actores involucrados en la conservación. 

     Este proyecto también fue posible gracias al contacto cercano con BirdLife, tanto con los coordinadores para latinoamérica así como con los equipos de ciencia de datos con quienes tuvimos contacto indirecto; BirdLife ha hecho un gran avance en tener la información sistematizada así como accesible para el público en general al que atienden, es decir, en formatos comunes para la transferencia de archivos. Las hojas de cálculo de criterios incluían toda la información necesaria para conformar una IBA a partir de una estimación poblacional.

     Después de revisar varias veces el camino emprendido, el mayor impedimento para la aprobación de los criterios ha sido el de la disponibilidad de datos: algunas IBAs contaban con muy pocos datos y otras con datos muy desactualizados; aún reduciendo dramáticamente la estrictez para eliminar las observaciones con probabilidad de ser la misma ave que utilizamos en este proyecto, no se habrían podido confirmar más allá de 150 IBAs en total. Se requiere urgentemente un mayor flujo de información de proyectos que no está curada en repositorios accesibles tales como eBird, así como una mejor distribución geográfica de las labores de monitoreo.

     Una importante mejora para aprovechar al máximo los esfuerzos de monitoreos que podría resultar en la aprobación de más IBAs es la incorporación de variables que permitan la estimación de densidades poblacionales, tales como la inclusión de información de distancia para ajustar curvas de probabilidad de detección. Esto lograría que una salida de campo desarrollada siguiendo un diseño de muestreo sirva para cuantificar las poblaciones de cierta especie en una IBA.

\pagebreak

## Referencias
::: {#refs}
:::

\pagebreak

## Descripción de los scripts utilizados:

### Preproceso
1. ebird_queries.sh: preproceso, eficiente en uso de memoria, usado para limpieza de duplicados, pre-proceso de columnas y unión de atributos del nombre de la IBA a los registros provenientes de eBird. El archivo resultante será el insumo principal para todos los siguientes scripts que incluyan datos de eBird.

### Criterios A1, A2 y A4
1. criterio_A1_revision.R: revisión del criterio A1, usando los valores por año o la suma de todo el periodo. Para datos de eBird.
1. criterio_A1_revision_aicas_no_calificadas.R: revisión del criterio A1, usando los valores por año o la suma de todo el periodo, para AICAs no calificadas como IBAs en revisiones previas. Para datos de eBird.
1. criterio_A2_revision.R: revisión criterio A2, usando conteos por año o la suma del periodo, para eBird.
1. criterio_A2_revision_aicas_no_calificadas.R: revisión del criterio A2, usando los valores por año o la suma de todo el periodo, para AICAs no calificadas como IBAs en revisiones previas. Para datos de ebird.
1. criterio_A4_ebird.R: revisión del criterio A4 con datos de ebird.
1. criterio_A4_ebird_no_calificadas.R: revisión del criterio A4 para AICAs no calificadas como IBAs en revisiones previas. Para datos de eBird.
1. criterio_A4_acuaticas.R: revisión del criterio A4 con datos de aves acuáticas.
1. criterio_A4_pronatura_noroeste.R: revisión del criterio A4 con datos de Pronatura Noroeste.
1. comparacion_A4.R: script para determinar cuál es el valor máximo por especie por IBA de las tres fuentes (eBird, aves acuáticas y Pronatura Noroeste) para incluir en la base de datos de BirdLife.

### Criterios B1, B3a y B3b
1. criterio_B1_revision.R: revisión del criterio B1 con datos de eBird.
1. criterio_B3a_acuaticas.R: revisión del criterio B3a con datos de aves acuáticas.
1. criterio_B3a_acuaticas_not_qualy.R: revisión del criterio B3a con datos de aves acuáticas, para AICAs no calificadas como IBAs en revisiones previas.
1. criterio_B3a_acuaticas.R: revisión del criterio B3a con datos de aves acuáticas.
1. criterio_B3a_acuaticas_no_calificadas.R: revisión del criterio B3a con datos de aves acuáticas, para AICAs no calificadas como IBAs en revisiones previas.
1. criterio_B3a_ebird.R: revisión del criterio B3a con datos de aves ebird.
1. criterio_B3a_ebird_no_calificadas.R: revisión del criterio B3a con datos de eBird para AICAs no calificadas como IBAs en revisiones previas.
1. criterio_B3a_pronor: revisión del criterio B3a con datos de Pronatura Noroeste.(nota: los registrosd de PRONOR no caen en AICAs no calificadas)
1. Archivo auxiliar pronor_b3_preproceso.R
1. criterio_B3b_ebird.R: revisión del criterio B3b con datos de aves ebird.
1. criterio_B3b_ebird_no_calificadas.R: revisión del criterio B3b con datos de eBird para AICAs no calificadas como IBAs en revisiones previas.
1. criterio_B3b_acuaticas.R: revisión del criterio B3b con datos de aves acuáticas.
1. criterio_B3b_acuaticas_no_calificadas.R: revisión del criterio B3b con datos de aves acuáticas, para AICAs no calificadas como IBAs en revisiones previas.
1. criterio_B3b_pronor: revisión del criterio B3b con datos de Pronatura Noroeste.(nota: los registrosd de PRONOR no caen en AICAs no calificadas)
1. B_quantile.R: Script auxiliares para determinación de "Frecuencia" de las especies para la base de BirdLife
1. B_quantile_NQ.R: Script auxiliares para determinación de "Frecuencia" de las especies para la base de BirdLife, para AICAs previamente no calificadas como IBAs.
1. concentra_xlsx.R: Genera una base de datos maestra con todos los resultados de los criterios B.





