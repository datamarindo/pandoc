# Archivo para guiar la creación/conversión de markdown/latex con Pandoc

Tiene los elementos principales: tabla de contenido, lista de figuras y de tablas, referencias, imágenes, énfasis, bibliografía con BIB

` pandoc -s -o informe.pdf informe.md --bibliography informe_bibtex.bib`
